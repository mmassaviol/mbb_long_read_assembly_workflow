#!/bin/bash

# This script is executed on the virtual machine during the *Deployment* phase.
# It is used to apply parameters specific to the current deployment.
# It is executed secondly during a cloud deployement in IFB-Biosphere, after the *Installation* phase.


APP_IMG="gitlab-registry.in2p3.fr/ifb-biosphere/apps/long_read_assembly:master"

# Tuning if site proxy or not
#CLOUD_SERVICE = $(ss-get cloudservice)
#CLOUD_SERVICE="ifb-genouest-genostack"

#HOST_NAME=$( ss-get --timeout=3 hostname )
HOST_NAME="192.168.100.49"
#if [ "$CLOUD_SERVICE" == "ifb-genouest-genostack" ]; then
    # Cloud site WITH a site proxy
#    APP_PORT=80
#    PROXIED_IP=$( echo $HOST_NAME | sed "s|\.|-|g")
#    HOST_NAME="openstack-${PROXIED_IP}.genouest.org"
#    HTTP_ENDP="https://$HOST_NAME"

#    systemctl stop nginx
#else
    # Cloud site WOUT a site proxy
    APP_PORT=8787
    HTTP_ENDP="https://$HOST_NAME"

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=FR/ST=AURA/L=Lyon/O=IFB/OU=IFB-biosphere/CN=myrstudio.biosphere.france-bioinformatique.fr"
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

    mkdir -p /etc/nginx/snippets
    echo "ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;" > /etc/nginx/snippets/self-signed.conf
    echo "ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;" >> /etc/nginx/snippets/self-signed.conf

    cp system/nginx_snippets_ssl-params.conf /etc/nginx/snippets/ssl-params.conf

    cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak

    cp system/nginx_sites-available_default /etc/nginx/sites-available/default
    sed -i "s|server_domain_or_IP|$HOST_NAME|"  /etc/nginx/sites-available/default

    useradd nginx
    cp system/nginx_nginx.conf /etc/nginx/nginx.conf

    cp system/nginx_conf.d_10-rstudio.conf /etc/nginx/conf.d/10-rstudio.conf
    sed -i "s|example.com|$HOST_NAME|" /etc/nginx/conf.d/10-rstudio.conf

    systemctl restart nginx
    systemctl enable nginx
#fi

# Docker volumes

# mydatalocal: from the system disk or ephemeral  one
IFB_DATADIR="/ifb/data/"
source /etc/profile.d/ifb.sh
VOL_NAME="mydatalocal"
VOL_DEV=$(readlink -f -n $IFB_DATADIR/$VOL_NAME )
DOCK_VOL=" --mount type=bind,src=$VOL_DEV,dst=$IFB_DATADIR/$VOL_NAME"

# NFS mounts: from ifb_share configuration in autofs
IFS_ORI=$IFS
while IFS=" :" read VOL_NAME VOL_TYPE VOL_IP VOL_DEV ; do
        DOCK_VOL+=" --mount type=volume,volume-driver=local,volume-opt=type=nfs,src=$VOL_NAME,dst=$IFB_DATADIR/$VOL_NAME,volume-opt=device=:$VOL_DEV,volume-opt=o=addr=$VOL_IP"
done < /etc/auto.ifb_share
IFS=$IFS_ORI

# MBB Workflows reads data from /Data and write results to /Results
mkdir ${VOL_DEV}/Data
mkdir ${VOL_DEV}/Results
DOCK_VOL+=" --mount type=bind,src=$VOL_DEV/Data,dst=/Data"
DOCK_VOL+=" --mount type=bind,src=$VOL_DEV/Results,dst=/Results"

# Run docker environment
# pull from docker hub or comment following 2 lines if using a local registry
#docker pull mmassaviol/long_read_assembly
APP_IMG="long_read_assembly"

CONTAINER_ID=$( docker run -d -p $APP_PORT:3838 $DOCK_VOL $APP_IMG )
#docker exec --user rstudio $CONTAINER_ID bash -c 'ln -s /ifb/data/ $HOME/data'
#docker exec --user rstudio $CONTAINER_ID bash -c 'rm -r $HOME/kitematic'

# Set service URLs
#echo $HTTP_ENDP
#echo $RST_PASSWORD
#ss-set url.service "${HTTP_ENDP}"
#ss-set ss:url.service "[HTTPS]$HTTP_ENDP"
