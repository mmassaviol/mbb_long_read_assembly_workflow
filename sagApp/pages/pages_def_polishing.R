tabpolishing = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectpolishing", label = "", value="medaka")),box(title = "medaka", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("medaka_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		selectInput("medaka_model", label = "Medaka model", choices = list("r941_trans" = "r941_trans", "r941_flip213" = "r941_flip213", "r941_flip235" = "r941_flip235", "r941_min_fast" = "r941_min_fast", "r941_min_high" = "r941_min_high", "r941_prom_fast" = "r941_prom_fast", "r941_prom_high" = "r941_prom_high"),  selected = "r941_min_high", width =  "auto"),

		p("medaka: Medaka is a tool to create a consensus sequence of nanopore sequencing data."),

		p("Website : ",a(href="https://github.com/nanoporetech/medaka","https://github.com/nanoporetech/medaka",target="_blank")),

		p("Documentation : ",a(href="https://nanoporetech.github.io/medaka/","https://nanoporetech.github.io/medaka/",target="_blank"))

	)))


