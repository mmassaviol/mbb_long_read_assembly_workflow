FROM rocker/r-ver:3.5.3

ENV PATH /opt/biotools/bin:$PATH
ENV ROOTSYS /opt/biotools/root
ENV LD_LIBRARY_PATH '$LD_LIBRARY_PATH:$ROOTSYS/lib'

RUN apt-get update
RUN apt-get install -yq tzdata
RUN apt-get install -y locales
RUN locale-gen "en_US.UTF-8"
RUN export LC_ALL=en_US.UTF-8
RUN export LANG=en_US.UTF-8

RUN apt-get install -y curl wget apt-utils
RUN apt-get install -y gcc fort77 aptitude
RUN aptitude install -y g++ xorg-dev libreadline-dev  gfortran
RUN apt-get install -y libssl-dev libxml2-dev libpcre3-dev liblzma-dev libbz2-dev libcurl4-openssl-dev liblapack3 git nano graphviz python3 python3-pip

RUN apt-get install -y  autotools-dev automake cmake grep sed dpkg fuse zip build-essential pkg-config bzip2 ca-certificates libglib2.0-0 libxext6 libsm6 libxrender1 mercurial subversion zlib1g-dev libncurses5-dev libncursesw5-dev

RUN apt-get clean

RUN if [ ! -d "/opt/biotools" ];then mkdir /opt/biotools; fi
RUN if [ ! -d "/opt/biotools/bin" ];then mkdir /opt/biotools/bin; fi
RUN chmod 777 -R /opt/biotools/
ENV PATH /opt/biotools/bin:$PATH

RUN pip3 install snakemake==5.4.0 oyaml
RUN pip3 install multiqc==1.7
RUN fic=$(find /usr/ -name stacks.py) && sed -i 's/out_dict\[s_name\] = cdict/out_dict\[content\[0\]\] = cdict/' $fic

RUN Rscript -e 'install.packages("yaml",Ncpus=8,repos="https://cloud.r-project.org/")'
RUN Rscript -e 'install.packages("DT",Ncpus=8,repos="https://cloud.r-project.org/")'
RUN Rscript -e 'install.packages("shiny",Ncpus=8,repos="https://cloud.r-project.org/")'
RUN Rscript -e 'install.packages("shinydashboard",Ncpus=8,repos="https://cloud.r-project.org/")'
RUN Rscript -e 'install.packages("shinyjs",Ncpus=8,repos="https://cloud.r-project.org/")'
RUN Rscript -e 'install.packages("shinyFiles",Ncpus=8,repos="https://cloud.r-project.org/")'


COPY files /workflow
COPY sagApp /sagApp

ENV PATH /opt/biotools/minimap2-2.17_x64-linux:$PATH 
RUN cd /opt/biotools \
 && wget https://github.com/lh3/minimap2/releases/download/v2.17/minimap2-2.17_x64-linux.tar.bz2 \
 && tar -xvjf minimap2-2.17_x64-linux.tar.bz2 \
 && rm minimap2-2.17_x64-linux.tar.bz2

ENV PATH /opt/biotools/miniasm-0.3:$PATH 
RUN cd /opt/biotools \
 && wget -O miniasm.tar.gz https://github.com/lh3/miniasm/archive/v0.3.tar.gz \
 && tar -xvzf miniasm.tar.gz \
 && cd miniasm-0.3 \
 && make -j 10 \
 && cd .. rm miniasm.tar.gz

ENV PATH /opt/biotools/racon-v1.4.10/bin:$PATH 
RUN cd /opt/biotools \
 && wget -O racon.tar.gz https://github.com/lbcb-sci/racon/releases/download/1.4.10/racon-v1.4.10.tar.gz \
 && tar -xvzf racon.tar.gz \
 && cd racon-v1.4.10 \
 && cmake -DCMAKE_BUILD_TYPE=Release \
 && make -j 10 \
 && cd .. \
 && rm racon.tar.gz

RUN wget -O htslib.tar.bz2 https://github.com/samtools/htslib/releases/download/1.9/htslib-1.9.tar.bz2 \
 && tar -xvjf htslib.tar.bz2 \
 && cd htslib-1.9 \
 && ./configure \
 && make -j 10 \
 && make install

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make -j 10 \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

RUN apt-get -y install tabix

RUN pip3 install medaka==0.10.1

RUN apt-get install -y zlib1g-dev pkg-config libfreetype6-dev libpng-dev python-matplotlib python-setuptools

RUN cd /opt/biotools \
 && wget https://github.com/ablab/quast/releases/download/quast_5.0.2/quast-5.0.2.tar.gz \
 && tar -zxvf quast-5.0.2.tar.gz \
 && cd quast-5.0.2/ \
 && python3 ./setup.py install_full

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]
