# Workflow MBB - Long_Read_Assembly

## Description

Workflow designed to assemble long reads, produce a consensus assembly and measure its quality. Can be used with a graphical interface in an internet browser.

## Tools

* minimap2
* miniasm
* racon
* medaka
* quast

## Contact

* [Mathieu Massaviol](http://www.isem.univ-montp2.fr/fr/personnel/plateformes/calcul-et-bioinformatique-mbb/massaviol-mathieu.index/), MBB platform

## Developpers

* [Mathieu Massaviol](http://www.isem.univ-montp2.fr/fr/personnel/plateformes/calcul-et-bioinformatique-mbb/massaviol-mathieu.index/), MBB platform

## App data

* Version : 0.0.1
* OS : debian
* OS version : buster
