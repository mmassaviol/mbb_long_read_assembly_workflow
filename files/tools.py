import oyaml as yaml

def read_yaml(filepath):
    try:
        with open(filepath, 'r') as file:
            data = yaml.load(file)
            return data
    except IOError as e:
        print("Error in file opening:", e)
    except yaml.YAMLError as exc:
        print("Error in yaml loading:", exc)

def write_yaml(filepath,data):
    try:
        with open(filepath, 'w') as file:
            yaml.dump(data, file, default_flow_style=False)
    except IOError as e:
        print("Error in file opening:", e)